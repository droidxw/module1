class EjemploFunciones{
	public static void saludo(int cuantas, String nombre){//Se ejecuta procedimiento
		System.out.println("Buenos dias a todos "+nombre);// Procedimiento por que no regresa valor
		int numero=0;
		for (numero=0; numero<cuantas; numero++){
			System.out.println(":-");
		}
		
	}	
	// Definici�n de funci�n suma
		public static double suma(double num1, double num2){//parametro entero
		double res=0;
		res=num1+num2;
		return(res);			
		
	}	
	
	public static void main(String [] args){
	
	System.out.println("Ejemplo de procedimientos");
	saludo(7, "H�ctor");//Se ejecuta procedimiento con par�metros
	saludo(9, "Pete");
	
	System.out.println("Ejemplo de funciones");
	double val=0;
	// Llamado a funci�n (subrutina)suma
	val=suma(5,9.3); 
	System.out.println("Resultado de la primera suma"+val);
	System.out.println("Resultado de la segunda suma"+suma(3,7));
	System.out.println("Resultado de la tercera suma"+suma(suma(7,6),12));
	
	}
}