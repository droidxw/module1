//Elaboro Hector Enrique de la Torre Pe�u�uri
//Ejemplo de creaci�n de instancias

import java.util.*;
class EjemploInstanciacion{
public static void main (String []args){

//**Instancias clase autos**//	
System.out.println("Ejemplo instancia de autos");

Auto a=new Auto();
a.setModelo("Aveo");	
a.setPuertas(-100);
a.setPrecio(300000);
		
System.out.println("Modelo "+a.getModelo()+" $"+a.getPrecio()+ " Puertas "+a.getPuertas()+"\n");	

// Auto a1=new Auto(4);


//**Instancias clase peliculas**//	
System.out.println("Ejemplo instancia de canciones");

//Constructor vacio
Cancion nu=new Cancion();
System.out.println(nu.titulo+" $"+nu.precio+" Duraci�n "+nu.duracion+" min"+" Genero: "+nu.genero);	
//Constructor con par�metro	
Cancion n1=new Cancion(56);
System.out.println(n1.titulo+" $"+n1.precio+" Duraci�n "+n1.duracion+" min"+" Genero: "+n1.genero);		

Cancion n2=new Cancion("");	
System.out.println(n2.titulo+" $"+n2.precio+" Duraci�n "+n2.duracion+" min"+" Genero: "+n2.genero+"\n");

//**Instancias clase peliculas**//
System.out.println("Ejemplo instancia de peliculas");
//Clase nombre = new Clase(parametros);
Pelicula una=new Pelicula();//una es una Pelicula
Pelicula dos=new Pelicula();
Pelicula tres=new Pelicula();

// Instancia Pelicula1
String estreno1="";
una.nombre="Suicide Squad";
una.duracion=100;
una.estrenada=false;
// if(una.estrenada==true){
// estreno1="Ya se estreno";
// }
// else
	// estreno1="No se ha estrenado";
estreno1=una.estrenada==true?"Ya se estreno":"No se ha estrenado";

// Instancia Pelicula2
String estreno2="";
dos.nombre="Payback";
dos.duracion=130;
dos.estrenada=true;
estreno2=dos.estrenada==true?"Ya se estreno":"No se ha estrenado";

// Instancia Pelicula3
//Llenar los otros dos atributos de ambas isntancias
//Solicitarte a quien ejecute los datos de la 3era peliculas//
String estreno3="";

System.out.println("Indica nombre de pel�cula:");
Scanner in=new Scanner (System.in);
tres.nombre=in.nextLine();

System.out.println("Indica duraci�n de la pel�cula:");
tres.duracion=in.nextInt();

System.out.println("Indica si ya se estreno:");
tres.estrenada=in.nextBoolean();

estreno3=tres.estrenada==true?"Ya se estreno":"No se ha estrenado";

System.out.println(una.nombre+" dura "+una.duracion+ " minutos "+estreno1);//imprime nombre de una
System.out.println(dos.nombre+" dura "+dos.duracion+ " minutos "+estreno2);
System.out.println(tres.nombre+" dura "+tres.duracion+ " minutos "+estreno3);
//Imprimir los atributos de 3, la parte estrenada mejorada
	}


}
