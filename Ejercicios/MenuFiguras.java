//Elaboro Hector Enrique de la Torre Pe�u�uri
import java.util.*;
class MenuFiguras{
	////M�todo menu que se invoca desde el switch////
		public static int menu(){
			
			int opcion=0;	
					
			System.out.println("1-Rectangulo");
			System.out.println("2-TrianguloRectangulo");
			System.out.println("3-TrianguloEscaleno");
			System.out.println("9-Salir");
			
		
			
			
			
			System.out.print("Seleccione # para imprimir figura ");
			Scanner in=new Scanner(System.in);
			
				try {
				//codigo que puede dispara excepciones
				
				opcion= in.nextInt();//Pide figura
				opcion=10/opcion;
				//si llega aqu�, todo estuvo bien
			}
			catch (InputMismatchException e){
				//algo que hara el programa cuando suceda  esta excepcion
				System.out.println("Solo se permiten n�meros enteros, vuelve a intentarlo");
				opcion=-99;
			}
			catch (ArithmeticException e){
				//algo que hara el programa cuando suceda  esta excepcion
				System.out.println("La division entre cero no es posible");
				opcion=-99;
			}
				catch (Exception e){
				//algo que hara el programa cuando suceda  esta excepcion
				System.out.println("Error desconocido, vuelve a intentarlo");
				opcion=-99;
			}
			finally{//siempre se ejecuta
			//por orden
			//Cierre del archivo abierto
			//cierras la conexion a la base de datos
				System.out.println("Cuando sale esto?")
			}
			//Aqui llega el programa haya o no haya excepcion
			return (opcion);
		
	}
	
			////M�todos por separado que se invocan desde el switch////
				public static void rectangulo(){
					System.out.println("Rectangulo\n");
					
					// Rectangulo objeto=new Rectangulo();
					// objeto.rectangulo();
				}		


				public static void trianguloR(){
						System.out.println("trianguloR\n");
						
					}	

				public static void trianguloE(){
						System.out.println("trianguloE\n");
						
					}
			
			////M�todos por separado que se invocan desde el switch////

	

public static void main (String[] args) {
	// System.out.println(menu());
	int figura=0;
	do{
					// System.out.print("Seleccione # para imprimir figura: \t");
					// Scanner in=new Scanner(System.in);
					// figura= in.nextInt();//Pide figura
					figura=menu();
					
							switch(figura){

										case 1:
										// System.out.println("Rectangulo");
										rectangulo();
										break;

										case 2:
										// System.out.println("TrianguloRectangulo");
										trianguloR();
										break;

										case 3:
										// System.out.println("TrianguloEscaleno");
										trianguloE();
										break;

										case 9:
										System.out.println("Fin del programa\n");
										break;
										
										default:
										System.out.println("Opci�n Invalida, haz otra selecci�n\n");

										
									}
										
				}while(figura!=9);
	




	}// Fin del main()
}
//Fin de la clase