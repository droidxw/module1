class Personaje{
	
	//Atributos
String nombre;
double vida;
double fuerza;
boolean vivo;
int intentos;

		//Constructor vac�o
		public Personaje(){
		this.nombre="Sin nombre";
		this.vida=100;
		this.fuerza=5;
		this.vivo=true;
		this.escudoActivado=false;
		this.intentos=3;
		

		}
		//Constructor con par�metros
		public Personaje(String nombreEntrada){
		this();//Constructor vacio
		this.nombre=nombreEntrada;
		}		
		
		//M�todos
		//Sobreescritura m�todo toString
		public String toString(){
		String regreso =this.nombre+"("+this.vida+")";
		return (regreso);
		}
		
		
		//M�todo  activaEscudo
		public void activaEscudo(){
			//Activa escudo
			this.escudoActivado=true;


		}
		
		//M�todo pega 
		public void pega(Personaje otroPersonaje){
			// vida - fuerza del golpe del otro personaje
			if(otroPersonaje.escudoActivado)
			otroPersonaje.vida-=this.fuerza/2;
		else
			otroPersonaje.vida-=this.fuerza;
if (otroPersonaje.vida<=0){//muerte personaje
	otroPersonaje.vida=100;
	otroPersonaje.intentos--;
}

		}
		
		


}