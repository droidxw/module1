//Elaboro Hector Enrique de la Torre Peñuñuri
//Ejemplificar uso de la clase scanner para adquirir datos del teclado

import java.util.*;

class EjemploScanner{
public static void main (String []args){
	Scanner teclado=new Scanner(System.in);//Instancia a scanner creando el objeto teclado
	
	//Datos de un libro
	
	String titulo="desc";
	String autor="desc";  //falta
	String impresion="01/01/2001";  //falta
 	int paginas=0;
	double precio=0; //promocion
	
	System.out.println("Datos del libro");
	
	System.out.print("Dame el titulo del libro");
	titulo=teclado.nextLine(); //Pide dato String
	
	System.out.print("Dame las paginas del libro");
	paginas=teclado.nextInt(); //Pide dato 
	
	System.out.print("Dame el precio del libro");
	precio=teclado.nextDouble();
		
	System.out.println("Libro"+ titulo);
	System.out.println("Autor(es)"+ autor);	
	System.out.println("Paginas"+ paginas);
	System.out.println("Precio"+ precio);
	System.out.println("Fecha de impresion"+ impresion);
	
	}
}