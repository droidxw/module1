
public class Venta extends Producto {
	
	private int cantidad;
	private double total;

	public Venta() {
		super();
		
		// TODO Auto-generated constructor stub
	}
	
	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public double getTotal(int cantidad,int tipo) {
		total=cantidad*this.getPrecioVta(tipo);
//		System.out.println(cantidad);
//		System.out.println(this.getPrecioVta(tipo));
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		boolean value=false;
			if (this == obj)
				value=true;
			if (obj == null)
				value=false;
			if (getClass() != obj.getClass())
				value=false;


		return value;
	}

}
