
//extends Cliente
public abstract class Producto   {
	
	
	  private  String nombre;	 
	  private  double precioCpa;
	  private double precioVta;
	  private  int existencias;
	  private  int clave;
	 	  


		public Producto() {
		super();		
		this.nombre = "Desc";
		this.precioCpa = -100;
		this.precioVta = -100;
		this.existencias = 0000;
		this.clave = 0000;
	}

	
	
	  
	 



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		//Se conectara a la B.D para hacer el insert
		this.nombre = nombre;
	}



	public double getPrecioCpa() {
		return precioCpa;
	}



	public void setPrecioCpa(double precioCpa) {
		//Se conectara a la B.D para hacer el insert
		this.precioCpa = precioCpa;
	}



//	public double getPrecioVta(String fabricante, String tipo) {
		public double getPrecioVta(int tipo) {
		if(tipo== 0021)
		this.precioVta=85000;
		if(tipo== 0022)
			this.precioVta=30000;
		if(tipo== 0023)
			this.precioVta=45000;
		return precioVta;
	}



	public void setPrecioVta(double precioVta) {
		//Se conectara a la B.D para hacer el insert
		this.precioVta = precioVta;
	}
	
	
	  public int getClave() {
		  
		return clave;
	}



	public void setClave(int clave) {
		//Se conectara a la B.D para hacer el insert
		this.clave = clave;
	}




	public int getExistencias(int tipo) {
		if(tipo== 0021)
			this.existencias=10;
		if(tipo== 0022)
			this.existencias=500;
		if(tipo== 0023)
			this.existencias=30;
			
		return this.existencias;
	}



	public void setExistencias(int existencias) {
		//Se conectara a la B.D para hacer el insert
		this.existencias = existencias;
	}




		
			
			@Override
	public String toString() {
		return "Producto [clave=" + clave + ", nombre=" + nombre + ", precioCpa=" + precioCpa + ", precioVta="
				+ precioVta + ", existencias=" + existencias + "]";
	}

			@Override
			public boolean equals(Object obj) {
				boolean value=false;
					if (this == obj)
						value=true;
					if (obj == null)
						value=false;
					if (getClass() != obj.getClass())
						value=false;
	

				return value;
			}



}
