
public class Provedor {
	
	private int idProvedor;
	private String denominacion;
	private String telefono;
	private String descripcion;

	public Provedor() {
		// TODO Auto-generated constructor stub
		this.idProvedor=-1;
		this.denominacion="Undefined";
		this.telefono="0000";
		this.descripcion="NA";
	}
	
	
	public int getIdProvedor() {
		return idProvedor;
	}

	public void setIdProvedor(int idProvedor) {
		//Se conectara a la B.D para hacer el insert
		this.idProvedor = idProvedor;
	}

	public String getDenominacion(int client) {
		if(client== 4)
			this.denominacion ="CALDERAS CHAVEZ SA";		
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		//Se conectara a la B.D para hacer el insert
		this.denominacion = denominacion;
	}


	public String getTelefono(int client) {
		if(client== 4)
			this.telefono="55436677";
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDescripcion(int client) {
		String dataClient;
		if(client== 4)	{
			this.descripcion ="Productos por surtir";
		dataClient=this.getDenominacion(client)+this.getTelefono(client)+" Nota: "+this.descripcion;
		}
		else
			dataClient="Sin definir ";
		return dataClient;
	}

	public String setDescripcion() {		
		//Se conectara a la B.D para hacer el insert
		return descripcion;
	}
		
		


	
	@Override
	public boolean equals(Object obj) {
		boolean value=false;
			if (this == obj)
				value=true;
			if (obj == null)
				value=false;
			if (getClass() != obj.getClass())
				value=false;


		return value;
	}

}
