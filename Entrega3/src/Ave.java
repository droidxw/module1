//final variable o clase inmutable no pueden heredar o modificar el valor
//se puede seguir instanciando (creando objetos)
public final class  Ave extends Animal {
boolean vuela;
int edad;
String nombre;
public static int estatico=10;
public int noestatico=30;

	public Ave() {
		this.vuela=true;
	}
	
	public void sonido() {
		System.out.println("El ave grazna...");
		
	}
	
	
	public  void come(String comida) {
		
		System.out.println("Contenido del método que era abstract");
	}
}
