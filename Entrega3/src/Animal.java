//public  class Animal {
public abstract class Animal {


	enum Pelaje {SINPELO, PELOCORTO, PELOREGULAR, PELOLARGO};
	String nombre;
	String raza;
	private double tamano;
	double edad;
	String color;
	char genero;
	String dueno;
	boolean carnivoro;
	Pelaje pelo;
	
	public abstract void come(String comida);
	

	
			public Animal() {
//				System.out.println("Construyendo animal");
				this.nombre="SN";
				this.raza="SR";
				this.tamano=0;
				this.carnivoro=true;
				//Atributo ENUM
				this.pelo=Pelaje.PELOCORTO;
				
			}
			
			
				
				public void setTamano(double nuevoTamano) {
					
					
					if(nuevoTamano >0 && nuevoTamano<=250)
						
						this.tamano=nuevoTamano;
					
				}	
				
				public double getTamano() {	
					return(this.tamano);
				
				}	
				
			
			
			
			public String toString() {
				return ("Nom="+this.nombre+", Raza="+this.raza+" C="+this.carnivoro+" /T= "+this.getTamano()+"Pelo "+this.pelo);
				
			}

			
			public void sonido() {
				System.out.println("El animal produce un sonido...");
				
			}

	}
