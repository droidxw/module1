//clase plantilla con atributos y métodos
public class Generica <T>  {//T debe ser una clase extends 
							//	trabaja con una clase no es de que sea subclase(hija) de la misma
T atributo;
int numero;
double precio;

public Generica (T valor) {
	this.atributo=valor;
	
}

	public void imprime (T objeto1) {
		System.out.println("Atributo"+this.atributo+"/"+objeto1);
		
   }
	
	public void repite(int numero) {
		for(int i=0;i<numero;i++) {
			System.out.println(this.atributo);
			
		}
	}

}
