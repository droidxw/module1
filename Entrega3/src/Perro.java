
public class Perro extends Animal {
	
	boolean esterilizado;
	String vacunas;
	
	
			public Perro() {
//				System.out.println("Construyendo perro");
				this.nombre="PSN";
				this.esterilizado=false;
				this.vacunas="PSV";
				this.setTamano(10);
			}
			
			
		public void setTamano(double nuevoTamano) {
				
				if(nuevoTamano >=0.5 && nuevoTamano<=30)
					
					super.setTamano(nuevoTamano);/// referencia a super clase Animal
				
			}
		
		
		public String toString() {
			String regreso=super.toString();
			regreso +=" Vacunas="+this.vacunas+" Esterilizado="+this.esterilizado;
			return(regreso);
			
		}
		
		
		
		public void sonido() {
			System.out.println("El perrro ladra...");
			
		}
		
		public void vacunarlo(String vacuna) {
			this.vacunas=vacuna;
			System.out.println("El perrro fue vacunado con"+this.vacunas);
			
		}

		

	public void come(String comida) {
		
		System.out.println("Contenido del método que era abstract");
	}

}
