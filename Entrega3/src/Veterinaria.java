
public class Veterinaria {
	
	//Lista finita de constantes(valores) definidos desde el inicio
	//sigue reglas de clases y los valores siguen reglas de constantes estaticos finales
	enum Tarifa{GRATUTIA, BASICA, CARA};
	enum Genero {FEMENINO, MASCULINO};
	
//	enum Mes{
//		ENE(31, "Enero"), FEB(28, "Febrero");
//			//se debe generar el constructor adecuado para el uso de ENUM
//		Mes(int i, String string) {
//			// TODO Auto-generated constructor stub
//		}	
//	};
	
	//enum tipo clase
	//define una enumeracion de 3 valores posibles
//	y les asocia un precio
	enum Corte{
		CHICO(100),MEDIANO(200), GRANDE(300);
		//enum puede tener atributos
		int costo;
		//enum puede tener constructores
		Corte(int precio){
			this.costo=precio;
		}
		//enum puede tener métodos (tipo estaticos)
		public int getCosto() {
			return(this.costo);
		}
	};
	
	public static void main(String[] args) {
//      Ejemplo de atributos estaticos 
//		Elementos estaticos los puedo utilizar mencionando la clase y el método  
//		sin necesidad de instanciar (uno para todos)
		Ave b1=new Ave();
		Ave b2=new Ave();
		
		b2.estatico=4;
		b2.noestatico=8;
		
		System.out.println(b1.noestatico+"-"+b1.estatico);	
		System.out.println(b2.noestatico+"-"+b2.estatico);	
		
		
		
		// TODO Auto-generated method stub
		System.out.println("Herencia ejemplo");
		
//		Animal a0=new Animal();
		Animal a1=new Perro();
		Perro p1=new Perro();//constructor
		p1.setTamano(20);
		
		System.out.println("Animal: "+a1);
		System.out.println("Perro: "+p1);		
		
//		assert variable >=0:"ERROR";
		
		System.out.println("Tarifas: ");
		//values() regresa todos los valores que puede tener tarifa
		Tarifa[] listaPrecios=Tarifa.values();
		Tarifa precio;
		
		for(int i=0; i<listaPrecios.length; i++) {
			//ordinal () regresa la posición que guarda dentro de la lista de valores posibles
			System.out.println("Nivel "+listaPrecios[i].ordinal()+"="+listaPrecios[i]);
			
		}
		String texto="BASICA";
		precio=Tarifa.valueOf(texto);//valueOf(String) regresa el elemento del enum que corresponde con el string
		System.out.println("Precio: "+ precio);
		
		//ejemplo de enum con constructor
		System.out.println("Tarifas: ");
		Corte objetoC=Corte.MEDIANO;
		//obeteniendo el valor mediante el constructor		
		System.out.println(objetoC+" $"+objetoC.getCosto());
		
		System.out.println("Polimorfismo ejemplo");
//		Perro[]grupo=new Perro[3];
		Animal[] grupo=new Animal[3];
//		al hacerse la clase Animal abstracta ya no puede ser instanciada
//		grupo[0]=new Animal();// nuevo Animal y lo guardo en grupo[0]
		grupo[0]=new Perro();
		grupo[1]=new Perro();// nuevo Perro y lo guardo en grupo[1]
		grupo[2]=new Ave();// nuevo Ave y lo guardo en grupo[2]
		
		for (int i=0; i<3; i++) {		
		System.out.println(grupo[i]);
		grupo[i].sonido();
		}
		
		
	}
	
	
	

}
