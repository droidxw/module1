
public interface IAjustesPantalla {
	public abstract int brilloMas();
	public abstract int brilloMenos();
	public abstract void ajustaBrillo(int nuevoBrillo);
}
