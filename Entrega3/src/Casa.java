
public class Casa {
//Atributos 
	private int pisos;
	public int ventanas;
	public int banos;
	public String direccion;
	public boolean conDueno;;
	
	//Métodos
	//Constructores
	public Casa() {
		
//		this.pisos=1;
		this.setPisos(3);
		this.setVentanas(1000);
//		this.ventanas=2;
		this.banos=1;
		this.direccion="Aún sin asignar";
		this.setConDueno(false);
//		this.conDueno=false;
	}
	

	//sets y gets
	//pisos
	public void setPisos(int nuevoValor) {//cambia el valor a pisos
		if(nuevoValor >0 && nuevoValor <=5)
		this.pisos=nuevoValor;//rango de uno a cinco se ejecuta la sentencia
//		return(this.getPisos()); //regreso el valor que le asigne en el set
	}
	
	public int getPisos() {//regresa el valor de pisos
		return(this.pisos);
		
		
	}	
	
	
	//ventanas
	public void setVentanas(int nuevoValor) {
		if(nuevoValor >=0 && nuevoValor <=20)
		this.ventanas=nuevoValor;//rango de uno a cinco
		else
			this.ventanas=2;

	}
	
	public int getVentanas() {
		return(this.ventanas);
		
		
	}
	
	//Dueño
	public void setConDueno(boolean nuevoValor) {
		this.conDueno=nuevoValor;
	}

	public String getConDueno() {
		return(this.conDueno?"Vendida":"Disponible");
	}
	
	////sets y gets////
	
	public String toString() {//representacion textual de mi objeto(en vez de la dirección de memoria)
		String valorTextual="Casa de"+this.getConDueno();
		valorTextual+="de"+this.getPisos()+"pisos";
		valorTextual+="Con direccion "+this.direccion;
		return(valorTextual);
		
	}	
	
	//sobreescritura del método equals
	public boolean equals (Object otro) {
		boolean valorRetorno=true;
		if(this==otro)//referencia (apuntan a la misma dirección de memoria)
			valorRetorno=true;
		else
			if(otro==null)//El objeto no esta inicializado(aún no se le asigna alguna instancia)
				valorRetorno=false;
			
		//evaluo si son de la misma clase
		else {
					//comparación de clases
					if (this.getClass()!=otro.getClass())//No son de la misma clase
						valorRetorno=false;
					
						else {//Aqui puedo comparar atributo a  atributo
								Casa nuevaCasa=(Casa)otro;//Creo objeto casa con lo que trae "otro" mediante un Cast
								if(this.getPisos()==nuevaCasa.getPisos()&&this.getConDueno()==nuevaCasa.getConDueno())
									valorRetorno=true;
								else
									valorRetorno=false;
						}
			}
	
		
		return (valorRetorno);
	}
	
	
//ordenar en base a criterio predefinido (posicion)
	//Sobreexscritura de compareTo
	public int compareTo(Casa otraCasa) {
		int regreso=0; //Son iguales (tienen el mismo número de pisos)
				if(this.getPisos()<otraCasa.getPisos())
					regreso=-1;//this esta antes
				else 
					if (this.getPisos()>otraCasa.getPisos())
						return regreso=1;//this esta después
			
		return(regreso);

	}
	
	
	
}
