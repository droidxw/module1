
public class EjemploGenericos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Generica<String>ogString =new Generica<String>("Hola");
		Generica<Double>ogDouble =new Generica<Double>(56.9);
		Generica<Ave>ogAve =new Generica<Ave>(new Ave());
		
		ogString.imprime("Ejemplo uso genéricos");
		ogDouble.imprime(100.1);
		ogAve.imprime(new Ave());

		ogString.repite(3);
	}

}
