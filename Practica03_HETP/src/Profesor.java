
public class Profesor extends Persona {	
	

	private String curp;
	private String academia;	
	
	//Constructoress//
	public Profesor() {
		curp="xxxx";
		academia="desconocida";
	
	}	
	
	
	
	//setters y getters
	public String getCurp() {
		return this.curp;
	}

	public void setCurp(String curp) {
		if( curp.length() ==18)
		this.curp = curp;
	}

	public String getAcademia() {
		return this.academia;
	}

	public void setAcademia(String academia) {
		if( academia.length() >=10)
		this.academia = academia;
	}
	
	public String toString() {
		return "Profesor [curp=" +this.curp + ", academia=" + this.academia + "]";
	}

	
	
	
	public boolean altaEnNomina() {
		System.out.println("Profesor registrado en nommina");
		return false;
		
	}
	
	
	


}
