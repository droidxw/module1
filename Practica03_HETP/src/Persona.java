
public class Persona {
	
	//Atributoss//
	private String nombre;
	private int edad;
	
	//Constructoress//
	public Persona() {

		this.nombre = "sinnombre";
		this.edad = 0;
	}
	
	public Persona(String nombre, int edad) {
		
		this.nombre = nombre;
		this.edad = edad;
	}

	
	
	//setters & getters//
	public String getNombre() {
		return this.nombre;
	}
	public void setNombre(String nombre) {
		
		if(nombre.length() >=10)	
		
		this.nombre = nombre;
	}
	public int getEdad() {
		return this.edad;
	}
	public void setEdad(int edad) {

		if(edad >0 && edad<=120)
		this.edad = edad;
	}
	
	
	
	public String toString() {
		return ("Nombre="+this.nombre+", Edad="+this.edad);
		
	}
	

//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//
//	}

}
